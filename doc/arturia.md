# Arturia devices

Notes about Arturia device support.

Arturia doesn't publish any MIDI implementation chart like it is
required when you obtain a manufacturer ID for system exclusive
messages. This speaks a lot.

Thanks to _untergeek_ for his reverse engineering effort:
<https://www.untergeek.de/2014/11/taming-arturias-beatstep-sysex-codes-for-programming-via-ipad/>

## SysEx Protocol

As of writing, the author has only one device, the _Arturia MiniLab
MkII_. Therefor all the observation are made based on that.

Most devices have a description file in the vendor driver (_MIDI
Control Center_). On macOS it is in `/Library/Arturia/MIDI Control
Center/Resources/` with a bunch of JSON file per device. This JSON
file contains important information to use for the protocol.

Byte values below are in hexadecimal.

Arturia has an extended SysEx vendor id of `20 6B`, the _Manufacturer
Systen Excluse id_ code is the following 3 bytes: `00 20 6B`
everywhere it's applicable.

The common format of the SysEx message is:

```text
F0 00 20 6B 7F 42 .... F7
   ^^^^^^^^ ^^ ^^
        |    |  \---------- The "productId" (JSON) for the device (hexstring)
        |    \------------- Device ID (7F is all)
        \------------------ Arturia SysEx ID (3 bytes)
```

- `42` is the `productId` in the JSON and seems to encompass a family
  of products to share the same protocol, in [`v2`].

### Device inquiry

The standard message for device inquiry is supported.
The software driver sends the standard device inquiry using the "wildcard" device ID of `7F`.

```text
F0 7E 7F 06 01 F7
```

The controller (MiniLab MkII) replies with the standard response

```text
       /--- Device ID
      vv
F0 7E 00 06 02 00 20 6B 02 00 04 02 43 07 00 01 F7
               ^^^^^^^^ ^^^^^ ^^^^^ ^^^^^^^^^^^
                      |   |     |
                      |   |     \------------------ Family ID
                      |   \------------------------ Family Member ID
                      \---------------------------- Arturia SysEx ID 3 bytes
```

| Field                   | Value |
|-------------------------|-------|
| SysEx ID                | `00 20 6B` Arturia SysEx ID  |
| Software revision level | `43 07 00 01` last 3 byte match firmware version |
| Family ID               | `familyId` in the JSON       |
| Family Member ID        | `familyMemberId` in the JSON |

## Generic Arturia SysEx

The number after the `:` is the number of bytes.

```text
<SysEx:6> <VERB:1> <ARG:1> [<PARAMID:1> <ITEMID:1> <VALUE:0-n>] F7
```

| Command name   | VERB | ARG |                                    |
|----------------|------|-----|------------------------------------|
| Query          | 01   | 00  | \<PARAMID:1> \<ITEMID:1> \<VALUE:0-n> |
| Report / Write | 02   | 00  | \<PARAMID:1> \<ITEMID:1> \<VALUE:0-n> |
| Read memory    | 05   | nn  |                                    |
| Write memory   | 06   | nn  |                                    |

Verb 02 is report when sent by the device, and write when sent by the
driver application. See [`v2::verb`] for the values.

PARAMID is the parameter to set or read. The can be the mode, the command, etc. See
[`v2::param`]. The meaning of PARAMID is dependent on context.

ITEMID is the ID of the control (CC)
VALUE is the value read or to write.

buttons or 00
otherwise the value of "deviceGlobalParametersId" for the global parameters of the device or "items.id" for the item / control.



## Button press event

Some button send SysEx commands. The report verb is used.

```text
<SysEx:6> 02 00 00 <ITEMID:1> <VALUE:1> F7
```

The `VALUE` byte contain the key state.

| State   | VALUE |
|---------|-------|
| Press   | 7F    |
| Depress | 00    |

The known keys are in the `ITEMID` byte.

| Key          | ITEMID |
|--------------|--------|
| Shift        | 2E     |
| Pad 1-8/9-16 | 2F     |
| Oct -        | 10     |
| Oct +        | 11     |

Note: there are possibly other keys on other devices.

## Devices

### MiniLab MkII

- `productId` = 42
- `familyId` = `02 00`
- `familyMemberId` = `04 02`

Firmware: 1.0.7

Can be software updated.

USB device:
- ID, strangely it has two different product ID.
  - 0x1c75:0x0289
  - 0x1c75:0x2209
- Name: Arturia MiniLab mkII
- Manufacturer: Arturia

Controls:
- 25 keys
- 8 pads (16 virtual)
- 16 rotary encoders
- programmable with SysEx: 8 memories (only 7 programmable)

### Factory reset

This will reset the programming
1. unplug USB
2. Hold down Oct - and Oct +
3. plug USB and keep holding until pads turn white
4. Reset is done once pads are off. (about 2 sec.)

### Unknown

- Can we query which octave is currently selected? Vendor software
  doesn't seem to do it, so it might not be possible.
