//
// (c) 2020 Hubert Figuière
//
// License: LGPL-3.0-or-later

//! Vendor specific MIDI support.

pub mod arturia;
